import {EdmMapping,EdmType} from '@themost/data/odata';
import RequestAction = require('./request-action-model');

/**
 * @class
 */
declare class RequestDocumentAction extends RequestAction {

     
     public documentCopies: number; 
     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 

}

export = RequestDocumentAction;