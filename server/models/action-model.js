import {EdmMapping} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import _ from "lodash";
import EnableAttachmentModel from "./enable-attachment-model";
import {DataConflictError} from "../errors";
import {DataNotFoundError} from "@themost/common";
import ActionStatusType from "./action-status-type-model";

/**
 * @class
 
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {number} result
 * @property {ActionStatusType|any} actionStatus
 * @property {EntryPoint|any} target
 * @property {User|any} owner
 * @property {User|any} agent
 * @property {Date} startTime
 * @property {Date} endTime
 * @property {Array<Account|any>} participants
 * @property {number} object
 * @property {number} error
 * @property {string} code
 * @property {Array<Attachment|any>} attachments
 * @property {Array<Account|any>} assignees
 * @property {Array<Account|any>} followers
 * @property {number} id
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 */
@EdmMapping.entityType('Action')
class Action extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    async addAttachment(file, extraAttributes) {
        // append extra attributes
        if (extraAttributes) {
            _.forEach(this.context.model('Attachment').attributeNames, (attribute)=> {
                if (Object.prototype.hasOwnProperty.call(extraAttributes, attribute)) {
                    Object.defineProperty(file, attribute, {
                        configurable: true,
                        enumerable: true,
                        writable: true,
                        value: extraAttributes[attribute]
                    })
                }
            });
        }
        return EnableAttachmentModel.prototype.addAttachment.call(this, file);
    }
    /**
     * Removes an attachment
     * @param {*} attachment
     */
    async removeAttachment(attachment) {
        /**
         * validate attachment
         * @type {DataObjectJunction}
         */
        const attachments = this.property('attachments');
        const itemAttachment = await attachments.where('id').equal(attachment.id).getItem();
        if (itemAttachment == null) {
            throw new DataConflictError('The specified attachment cannot be found or is inaccessible');
        }
        await EnableAttachmentModel.prototype.removeAttachment.call(this, attachment.id);
        return itemAttachment;
    }
    /**
     * Allows current user to claim an active action
     */
    async claim() {
        // validate action status
        const item = await this.getModel()
            .where('id').equal(this.getId())
            .select('agent','actionStatus')
            .getItem();
        if (item == null) {
            throw new DataNotFoundError('Action not found or is inaccessible.', null, this.getModel().name);
        }
        if (item.actionStatus.alternateName !== ActionStatusType.ActiveActionStatus) {
            throw new DataConflictError('Action cannot be claimed due to its state.', null, this.getModel().name);
        }
        if (item.agent != null) {
            throw new DataConflictError('Current action has been already claimed.', null, this.getModel().name);
        }
        // do update
        this.agent = {
            name: this.context.user.name
        };
        await this.save();
    }
}
module.exports = Action;
