import {EdmMapping} from '@themost/data';
let Action = require('./action-model');
/**
 * @class
 * @property {number} id
 */
@EdmMapping.entityType('UpdateAction')
class UpdateAction extends Action {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = UpdateAction;
