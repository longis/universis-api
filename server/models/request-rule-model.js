import { EdmMapping } from '@themost/data';
import Rule from './rule-model';

@EdmMapping.entityType('RequestRule')
class RequestRule extends Rule {

}

export default RequestRule;