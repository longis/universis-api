import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import StudentPeriodRegistration = require('./student-period-registration-model');
import User = require('./user-model');
import DocumentStatus = require('./document-status-model');

/**
 * @class
 */
declare class StudentRegistrationDocument extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός της εγγραφής.
      */
     public id: number;
     
     /**
      * @description Student Registration
      */
     public registration: StudentPeriodRegistration|any; 
     
     /**
      * @description Ο συνδεδεμένος χρήστης
      */
     public user?: User|any; 
     
     /**
      * @description Περιγραφή του document
      */
     public documentSubject?: string; 
     
     /**
      * @description Xml document
      */
     public originalDocument?: string; 
     
     /**
      * @description The document results
      */
     public documentResults?: string; 
     
     /**
      * @description Document status
      */
     public documentStatus?: DocumentStatus|any; 
     
     /**
      * @description Document status reason
      */
     public documentStatusReason?: string; 
     
     /**
      * @description Document checksum key
      */
     public checkHashKey?: string; 
     
     /**
      * @description Created date
      */
     public dateCreated?: Date; 
     
     /**
      * @description dateModified
      */
     public dateModified: Date; 

}

export = StudentRegistrationDocument;