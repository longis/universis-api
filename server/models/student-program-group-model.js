import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {round} from "mathjs";

/**
 * @class
 
 * @property {string} id
 * @property {ProgramGroup|any} programGroup
 * @property {Student|any} student
 * @property {AcademicYear|any} gradeYear
 * @property {AcademicPeriod|any} gradePeriod
 * @property {Semester|any} semester
 * @property {number} specialty
 * @property {number} units
 * @property {number} coefficient
 * @property {number} grade
 * @property {string} gradePeriodDescription
 * @property {CourseType|any} courseType
 * @property {boolean} calculateUnits
 * @property {*} calculateGrade
 * @property {*} calculateGroupGrade
 * @property {number} ects
 * @property {number} minCourses
 * @property {number} maxCourses
 * @property {number} isPassed
 * @property {Date} dateModified
 * @property {string} formattedGrade
 * @augments {DataObject}
 */
@EdmMapping.entityType('StudentProgramGroup')
class StudentProgramGroup extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    async calculate() {
        const self = this, context = self.context;
        try {
            // get student
            const student = self.student.id || self.student;
            const programGroupId = self.programGroup.id | self.programGroup;
            if (self.calculateGroupGrade === 1) {
                const studentData = await context.model('Student').where("id").select('id','department').equal(student).flatten().silent().getItem();
                //get programGroupDecimalDigits from student department
                const programGroupDecimalDigits = await context.model('LocalDepartment').where('id').equal(studentData.department).select('programGroupDecimalDigits').silent().value();
                const programGroup = await context.model('ProgramGroup').where('id').equal(programGroupId).expand({
                    'name': 'gradeScale',
                    'options': {
                        '$expand': 'values'
                    }})
                    .silent().getItem();

               if (programGroup) {
                   // get program group courses and calculate grade
                   const groupCourses = await context.model('StudentCourse').where('student').equal(student)
                       .and('programGroup').equal(programGroupId)
                       .and('isPassed').equal(1)
                       .and('courseStructureType').in([1, 4])
                       .and('calculated').equal(1)
                       .orderByDescending('gradeYear').thenByDescending('gradePeriod')
                       .silent()
                       .getItems();
                   if (groupCourses.length >= self.maxCourses) {
                       // calculate student group grade
                       const percent = groupCourses.reduce((partial_sum, a) => partial_sum + a.groupPercent, 0);
                       self.grade = percent > 0 ? round(groupCourses.reduce((partial_sum, a) => partial_sum + a.grade * a.groupPercent, 0) / percent, programGroupDecimalDigits + 1) : 0;
                       /**
                        * @GradeScale
                        */
                       const studentGroupGradeScale = context.model('GradeScale').convert(programGroup.gradeScale);
                       if (studentGroupGradeScale.scaleType !== 0 || studentGroupGradeScale.step>0) {
                           //use gradeScale to get the exact value for gradeScales with values e.g. 0-10 step 0.5
                           self.grade = studentGroupGradeScale.convertTo(self.grade);
                           self.grade = studentGroupGradeScale.convertFrom(self.grade);
                       }
                       self.formattedGrade = studentGroupGradeScale.convertTo(self.grade);
                       self.isPassed = 1;
                       self.gradeYear = groupCourses[0].gradeYear;
                       self.gradePeriod = groupCourses[0].gradePeriod;
                   }
                   else
                   {
                       self.formattedGrade = null;
                       self.grade = null;
                       self.isPassed = 0;
                       self.gradeYear = null;
                       self.gradePeriod = null;
                   }
               }
            }
            return self;
        } catch (e) {
            throw (e);
        }
    }
}
module.exports = StudentProgramGroup;
