import {EdmMapping, EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {DataError} from "@themost/common";

/**
 * @class

 * @property {number} id
 * @property {Department|any} department
 * @property {string} name
 * @property {Date} startDate
 * @property {AcademicYear|any} startYear
 * @property {AcademicPeriod|any} startPeriod
 * @property {Date} endDate
 * @property {Instructor|any} instructor
 * @property {number} coefficient
 * @property {string} notes
 * @property {number} subject
 * @property {string} otherInstructors
 * @property {ThesisType|any} type
 * @property {ThesisStatus|any} status
 * @property {number} grade
 * @property {number} isPassed
 * @property {string} formattedGrade
 * @property {number} units
 * @property {GradeScale|any} gradeScale
 * @property {number} ects
 * @property {Date} examDate
 * @property {Date} dateModified
 * @property {Array<StudentThesis|any>} students
 * @augments {DataObject}
 */
@EdmMapping.entityType('Thesis')
class Thesis extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Updates thesis roles
     * @returns {*}
     */
    @EdmMapping.param('data', EdmType.CollectionOf('ThesisRole'), false, true)
    @EdmMapping.action('roles',EdmType.CollectionOf('ThesisRole'))
    async setRoles(data) {
        try {
            // check if data contains all thesisRoles
            //get thesis committee
            let thesisRoles = await this.context.model('ThesisRole')
                .where('thesis').equal(this.id)
                .getItems();
            // map data instructor with results
            thesisRoles = thesisRoles.map(x => {
                const item = data.find(y => {
                    return y.member === x.member;
                });
                if (item) {
                    x.factor = item.factor;
                    x.roleName = item.roleName;
                } else {
                    throw new DataError('Thesis role is missing');
                }
                return x;
            });
            const isValid = await this.validateFactors(thesisRoles);
            if (isValid) {
                await this.context.model('ThesisRole').save(thesisRoles);
                return thesisRoles;
            } else {
                throw new DataError(this.context.__('Coefficients of thesis members are invalid'));
            }

        } catch (e) {
            throw e;

        }
    }

    async validateFactors(data) {
        // get role members
        const self = this, context = self.context;
        try {
            let results;

            if (data) {
                results = data;
            } else {
                // get thesis roles
                results = await context.model('ThesisRole')
                    .where('thesis').equal(self.id)
                    .getItems();
            }
            const percent = results.reduce((partial_sum, a) => partial_sum + a.factor, 0);
            if (Math.round(percent*100) / 100 === 1 || Math.round(percent*100) / 100 === 100) {
                return true;
            }
            // check also if factors are the same
            let factorArray = [...results.filter(x=>{return x.factor>0}).map(item => item.factor)];
            factorArray = factorArray.filter(function (item, index, inputArray) {
                return inputArray.indexOf(item) === index;
            });
            return factorArray.length === 1 ? true : false;
        } catch (e) {
            throw e;
        }
    }
}

module.exports = Thesis;
