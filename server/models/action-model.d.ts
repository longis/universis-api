import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import ActionStatusType = require('./action-status-type-model');
import EntryPoint = require('./entry-point-model');
import User = require('./user-model');
import Account = require('./account-model');
import Attachment = require('./attachment-model');

/**
 * @class
 */
declare class Action extends DataObject {

     
     public additionalType: string; 
     
     public alternateName?: string; 
     
     /**
      * @description The result produced in the action. e.g. John wrote a book.
      */
     public result?: number; 
     
     /**
      * @description Indicates the current disposition of the Action.
      */
     public actionStatus?: ActionStatusType; 
     
     /**
      * @description Indicates a target EntryPoint for an Action.
      */
     public target?: EntryPoint; 
     
     /**
      * @description The owner of the action e.g. a user who request something.
      */
     public owner?: User; 
     
     /**
      * @description The direct performer or driver of the action (animate or inanimate). e.g. <em>John</em> wrote a book.
      */
     public agent?: User; 
     
     /**
      * @description The startTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to start. For actions that span a period of time, when the action was performed. e.g. John wrote a book from <em>January</em> to December.</p>

<p>Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.
      */
     public startTime?: Date; 
     
     /**
      * @description The endTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to end. For actions that span a period of time, when the action was performed. e.g. John wrote a book from January to <em>December</em>.</p>

<p>Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.
      */
     public endTime?: Date; 
     
     /**
      * @description Other co-agents that participated in the action indirectly. e.g. John wrote a book with <em>Steve</em>.
      */
     public participants?: Array<Account>; 
     
     /**
      * @description The object upon which the action is carried out, whose state is kept intact or changed. Also known as the semantic roles patient, affected or undergoer (which change their state) or theme (which doesn't). e.g. John read <em>a book</em>.
      */
     public object?: number; 
     
     /**
      * @description For failed actions, more information on the cause of the failure.
      */
     public error?: number; 
     
     /**
      * @description A sequence of alphanumeric characters which represents a random code associated with this this action.
      */
     public code: string; 
     
     /**
      * @description A collection of attachments associated with this action.
      */
     public attachments?: Array<Attachment>; 
     
     /**
      * @description An assignee is a user or group of users responsible for doing the action.
      */
     public assignees?: Array<Account>; 
     
     /**
      * @description A follower is anyone who wants to be kept  in the loop as the action progresses.
      */
     public followers?: Array<Account>; 
     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 
     
     /**
      * @description Μία σύντομη περιγραφή για το στοιχείο.
      */
     public description?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση URL της εικόνας του στοιχείου.
      */
     public image?: string; 
     
     /**
      * @description Το όνομα του στοιχείου.
      */
     public name?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση με περισσότερες πληροφορίες για το στοιχείο. Π.χ. τη διεύθυνση URL της σελίδας Wikipedia ή του επίσημου ιστότοπου.
      */
     public url?: string; 
     
     /**
      * @description Η ημερομηνία δημιουργίας του στοιχείου
      */
     public dateCreated?: Date; 
     
     /**
      * @description Η ημερομηνία τροποποίησης του στοιχείου
      */
     public dateModified?: Date; 
     
     /**
      * @description Ο χρήστης που δημιούργησε το στοιχείο.
      */
     public createdBy?: User|any; 
     
     /**
      * @description Ο χρήστης που τροποποίησε το στοιχείο.
      */
     public modifiedBy?: User|any; 

}

export = Action;