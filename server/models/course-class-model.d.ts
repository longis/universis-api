import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Course = require('./course-model');
import AcademicYear = require('./academic-year-model');
import AcademicPeriod = require('./academic-period-model');
import ClassStatus = require('./class-status-model');
import CourseClassSection = require('./course-class-section-model');
import CourseClassInstructor = require('./course-class-instructor-model');

/**
 * @class
 */
declare class CourseClass extends DataObject {

     
     /**
      * @description Ο κωδικός της τάξης του μαθήματος
      */
     public id: string; 
     
     /**
      * @description Το μάθημα στο οποίο αναφέρεται η τάξη.
      */
     public course: Course|any; 
     
     /**
      * @description Ακαδημαϊκό έτος διδασκαλίας
      */
     public year: AcademicYear|any; 
     
     /**
      * @description Ακαδημαϊκή περίοδος διδασκαλίας
      */
     public period: AcademicPeriod|any; 
     
     /**
      * @description Κατάσταση τάξης
      */
     public status: ClassStatus|any; 
     
     /**
      * @description Ημερομηνία αλλαγής κατάστασης
      */
     public statusModified?: Date; 
     
     /**
      * @description Μέγιστος αριθμός φοιτητών που δέχεται η τάξη
      */
     public maxNumberOfStudents?: number; 
     
     /**
      * @description Ελάχιστος αριθμός φοιτητών που δέχεται η τάξη
      */
     public minNumberOfStudents?: number; 
     
     /**
      * @description Ώρες διδασκαλίας (ανά εβδομάδα)
      */
     public weekHours?: number; 
     
     /**
      * @description Ο συνολικός αριθμός των ωρών που διδάσκεται το μάθημα την ακαδημαϊκή περίοδο
      */
     public totalHours?: number; 
     
     /**
      * @description Όριο απουσιών
      */
     public absenceLimit?: number; 
     
     /**
      * @description Αριθμός φοιτητών που δήλωσαν την τάξη
      */
     public numberOfStudents?: number; 
     
     /**
      * @description Τίτλος τάξης (μπορεί να διαφέρει ανά έτος)
      */
     public title?: string; 
     
     /**
      * @description Υποχρεωτική επιλογή τμήματος τάξης κατά τη δήλωση
      */
     public mustRegisterSection?: boolean; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 
     
     /**
      * @description Το σύνολο των τμημάτων της τάξης.
      */
     public sections?: Array<CourseClassSection|any>; 
     
     /**
      * @description Το σύνολο των διδασκόντων της τάξης.
      */
     public instructors?: Array<CourseClassInstructor|any>;

     getRegistrationRules(): Promise<any>;
     setRegistrationRules(items: any[]): Promise<any>;

     getSectionRegistrationRules(): Promise<any>;
     setSectionRegistrationRules(items: any[]): Promise<any>;

}

export = CourseClass;
