import { DataError, DataNotFoundError, TraceUtils } from '@themost/common';
import { DataObject } from '@themost/data/data-object';
import { EdmMapping } from '@themost/data/odata';

@EdmMapping.entityType('StudentSuspension')
/**
 * @class
 * @augments {DataObject}
 */
class StudentSuspension extends DataObject {
	constructor() {
		super();
	}

	async isLastSuspension() {
		const context = this.context;
		const self = this.getId();
		const StudentSuspensions = context.model('StudentSuspension');
		let student;
		// get student
		if (this.student) {
			student = this.student;
		} else {
			student = await StudentSuspensions.where('id')
				.equal(self)
				.select('student')
				.silent()
				.value();
		}
		// validate student
		if (student == null) {
			throw new DataError(
				'E_STUDENT',
				'The student cannot be found.',
				null,
				'StudentSuspension',
				'student'
			);
		}
		// get all student suspensions
		const suspensions = await StudentSuspensions.where('student')
			.equal(student.id || student)
			.select('id', 'identifier')
			.silent()
			.getAllItems();
		// validate array structure and length
		if (!(Array.isArray(suspensions) && suspensions.length)) {
			throw new DataError(
				'E_SUSPENSIONS',
				'The student suspensions cannot be found.'
			);
		}
		if (suspensions.length === 1) {
			return true;
		}
		// sort by identifer (custom because of string type)
		suspensions.forEach((suspension) => {
			if (suspension.identifier && typeof suspension.identifier === 'string') {
				suspension.identifier = suspension.identifier.replace(/_/g, '');
			}
		});
		suspensions.sort((acc, next) => {
			return acc.identifier - next.identifier;
		});
		return self === suspensions[suspensions.length - 1].id;
	}

	@EdmMapping.action('delete', 'StudentSuspension')
	async delete() {
		try {
			// Note: All removal validations will happen at the before-remove listener
			// of the student suspension object. No need to validate at this point.
			const context = this.context;
			const StudentSuspendActions = context.model('StudentSuspendAction');
			// try to find if the suspension was created from an action
			const studentSuspendAction = await StudentSuspendActions.where(
				'studentSuspension'
			)
				.equal(this.getId())
				.and('actionStatus/alternateName')
				.equal('CompletedActionStatus')
				.select('id')
				.silent()
				.getItem();
			// if it was
			if (studentSuspendAction) {
				studentSuspendAction.actionStatus = {
					alternateName: 'CancelledActionStatus',
				};
				// cancel action and trigger its listener (will remove the suspension)
				await StudentSuspendActions.save(studentSuspendAction);
				// and exit
				return this;
			}
			// otherwise, just proceed with removing the student suspension
			await context.model('StudentSuspension').remove({ id: this.getId() });
			return this;
		} catch (err) {
			TraceUtils.error(err);
			throw err;
		}
	}
}

module.exports = StudentSuspension;
