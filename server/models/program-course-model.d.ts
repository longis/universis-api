import {DataObject} from '@themost/data/data-object';
import StudyProgram = require('./study-program-model');
import Course = require('./course-model');
import ProgramGroup = require('./program-group-model');
import Semester = require('./semester-model');
import CourseType = require('./course-type-model');

/**
 * @class
 */
declare class ProgramCourse extends DataObject {

     
     /**
      * @description Ο κωδικός του μαθήματος προγράμματος σπουδών
      */
     public id: number; 
     
     /**
      * @description Πρόγραμμα σπουδών
      */
     public program: StudyProgram|any; 
     
     /**
      * @description Μάθημα
      */
     public course: Course|any; 
     
     /**
      * @description Η ομάδα που ανήκει το μάθημα στο πρόγραμμα σπουδών.
      */
     public programGroup?: ProgramGroup|any; 
     
     /**
      * @description Το ποσοστό συμμετοχής του βαθμού του συγκεκριμένου μαθήματος στην ομάδα
      */
     public groupPercent?: number; 
     
     /**
      * @description Η κατεύθυνση του προγράμματος σπουδών
      */
     public specialty: number; 
     
     /**
      * @description Εξάμηνο
      */
     public semester: Semester|any; 
     
     /**
      * @description Συντελεστής πτυχίου
      */
     public coefficient: number; 
     
     /**
      * @description Τύπος μαθήματος
      */
     public courseType: CourseType|any; 
     
     /**
      * @description Διδακτικές μονάδες
      */
     public units?: number; 
     
     /**
      * @description Μονάδες ECTS
      */
     public ects?: number; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date;

     getRegistrationRules(): Promise<any>;
     setRegistrationRules(items: any[]): Promise<any>;

}

export = ProgramCourse;
