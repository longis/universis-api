import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Department = require('./department-model');

/**
 * @class
 */
declare class CourseArea extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός του γνωστικού αντικειμένου
      */
     public id: number; 
     
     /**
      * @description Η ονομασία του γνωστικού αντικειμένου
      */
     public name: string; 
     
     /**
      * @description Η συντομογραφία του γνωστικού αντικειμένου
      */
     public abbreviation?: string; 
     
     /**
      * @description Κωδικός τμήματος
      */
     public department: Department|any; 

}

export = CourseArea;