import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class LocalSettings extends DataObject {

     
     public id: number; 
     
     /**
      * @description Ονομασία ρύθμισης.
      */
     public name?: string; 
     
     /**
      * @description Πρώτη ημερομηνία που μπορεί να χρησιμοποιείται σε μια ρύθμιση
      */
     public date1?: Date; 
     
     /**
      * @description Δεύτερη ημερομηνία που μπορεί να χρησιμοποιείται σε μια ρύθμιση
      */
     public date2?: Date; 
     
     /**
      * @description Πρώτος αριθμός που μπορεί να χρησιμοποιείται σε μια ρύθμιση
      */
     public int1?: number; 
     
     /**
      * @description Δεύτερος αριθμός που μπορεί να χρησιμοποιείται σε μια ρύθμιση
      */
     public int2?: number; 
     
     /**
      * @description Πρώτο κείμενο που μπορεί να χρησιμοποιείται σε μια ρύθμιση
      */
     public text1?: string; 
     
     /**
      * @description Δεύτερο κείμενο που μπορεί να χρησιμοποιείται σε μια ρύθμιση
      */
     public text2?: string; 

}

export = LocalSettings;