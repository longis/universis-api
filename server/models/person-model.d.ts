import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Gender = require('./gender-model');
import Region = require('./region-model');
import IdentityType = require('./identity-type-model');
import FamilyStatus = require('./family-status-model');
import Nationality = require('./nationality-model');
import Country = require('./country-model');

/**
 * @class
 */
declare class Person extends DataObject {

     
     /**
      * @description Ο κωδικός της εγγραφής
      */
     public id: number; 
     
     /**
      * @description Το επώνυμο της επαφής
      */
     public familyName: string; 
     
     /**
      * @description Το όνομα της επαφής
      */
     public givenName: string; 
     
     /**
      * @description Φύλο
      */
     public gender: Gender|any; 
     
     /**
      * @description Το πατρώνυμο της επαφής
      */
     public fatherName?: string; 
     
     /**
      * @description Το μητρώνυμο της επαφής
      */
     public motherName?: string; 
     
     /**
      * @description Όνομα συζύγου
      */
     public spouseName?: string; 
     
     /**
      * @description Αριθμός δημοτολογίου
      */
     public citizenRegistrar?: string; 
     
     /**
      * @description Τόπος δημοτολογίου
      */
     public citizenRegistrarPlace?: string; 
     
     /**
      * @description Νομός τόπου δημοτολογίου
      */
     public citizenRegistrarRegion?: Region|any; 
     
     /**
      * @description Αριθμός μητρώου αρρένων
      */
     public maleRegistrar?: string; 
     
     /**
      * @description Τόπος μητρώου αρρένων
      */
     public maleRegistrarPlace?: string; 
     
     /**
      * @description Νομός τόπου μητρώου αρρένων
      */
     public maleRegistrarRegion?: Region|any; 
     
     /**
      * @description Αριθμός δελτίου ταυτότητας ή διαβατηρίου
      */
     public identityCard?: string; 
     
     /**
      * @description Τύπος ταυτότητας
      */
     public identityType?: IdentityType|any; 
     
     /**
      * @description Ημερομηνία έκδοσης ταυτότητας
      */
     public identityDate?: Date; 
     
     /**
      * @description Αρχή έκδοσης ταυτότητας
      */
     public identityAuthority?: string; 
     
     /**
      * @description Αριθμός φορολογικού μητρώου
      */
     public vatNumber?: string; 
     
     /**
      * @description Δημόσια Οικονομική Υπηρεσία (Δ.Ο.Υ)
      */
     public vatOffice?: string; 
     
     /**
      * @description Ημερομηνία γέννησης
      */
     public birthDate?: Date; 
     
     /**
      * @description Τόπος γέννησης
      */
     public birthPlace?: string; 
     
     /**
      * @description Νομός τόπου γέννησης
      */
     public birthPlaceRegion?: Region|any; 
     
     /**
      * @description Οικογενειακή κατάσταση
      */
     public familyStatus?: FamilyStatus|any; 
     
     /**
      * @description Αριθμός τέκνων
      */
     public children?: number; 
     
     /**
      * @description Διεύθυνση ηλεκτρονικού ταχυδρομείου επικοινωνίας
      */
     public email?: string; 
     
     /**
      * @description Υπηκοότητα
      */
     public nationality?: Nationality|any; 
     
     /**
      * @description Σημειώσεις
      */
     public notes?: string; 
     
     /**
      * @description Το πατρώνυμο στη γενική
      */
     public fatherNameGenitive?: string; 
     
     /**
      * @description Το μητρώνυμο στη γενική
      */
     public motherNameGenitive?: string; 
     
     /**
      * @description Η διεύθυνση μόνιμης κατοικίας
      */
     public homeAddress?: string; 
     
     /**
      * @description ΤΚ διεύθυνσης μόνιμης κατοικίας
      */
     public homePostalCode?: string; 
     
     /**
      * @description Η πόλη της διεύθυνσης μόνιμης κατοικίας
      */
     public homeCity?: string; 
     
     /**
      * @description Η χώρα της διεύθυνσης μόνιμης κατοικίας
      */
     public homeCountry?: Country|any; 
     
     /**
      * @description Τηλέφωνο διεύθυνσης μόνιμης κατοικίας
      */
     public homePhone?: string; 
     
     /**
      * @description Νομός διεύθυνσης μόνιμης κατοικίας
      */
     public homeAddressRegion?: Region|any; 
     
     /**
      * @description Η διεύθυνση προσωρινής κατοικίας
      */
     public temporaryAddress?: string; 
     
     /**
      * @description ΤΚ διεύθυνσης προσωρινής κατοικίας
      */
     public temporaryPostalCode?: string; 
     
     /**
      * @description Η πόλη της διεύθυνσης προσωρινής κατοικίας
      */
     public temporaryCity?: string; 
     
     /**
      * @description Η χώρα της διεύθυνσης προσωρινής κατοικίας
      */
     public temporaryCountry?: Country|any; 
     
     /**
      * @description Τηλέφωνο διεύθυνσης προσωρινής κατοικίας
      */
     public temporaryPhone?: string; 
     
     /**
      * @description Νομός διεύθυνσης προσωρινής κατοικίας
      */
     public temporaryAddressRegion?: Region|any; 
     
     /**
      * @description Αριθμός Μητρώου Κοινωνικής Ασφάλισης (ΑΜΚΑ)
      */
     public SSN?: string; 
     
     /**
      * @description Κινητό τηλέφωνο
      */
     public mobilePhone?: string; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 
     
     /**
      * @description Το ονοματεπώνυμο της επαφής
      */
     public name?: string; 

}

export = Person;