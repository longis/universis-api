import request from 'supertest';
import {assert} from 'chai';
import app from '../app';
import passport from 'passport';
import {TestBearerStrategy} from '../utils';
import morgan from 'morgan';
import indexRouter from '../routes/index';
import {writeAccessLog} from '../log';

const STUDENTS_BEARER = new TestBearerStrategy({
    "name": "student1@example.com",
    "authenticationType":"Bearer",
    "authenticationToken": "test-api-server-token==",
    "authenticationScope": "students"
});

describe('morgan', () => {

    it('GET /test-internal-request', async () => {
        indexRouter.get('/test/request',
            /**
             * @param {e.Request} req
             * @param {e.Response} res
             * @param next
             */
            (req, res, next) => {
            const _startAt = process.hrtime();
            const fakeReq = Object.assign({
                headers: req.headers,
                method: req.method,
                url: '/token/info',
                referrer: req.referrer,
                connection: {
                    remoteAddress: req.connection.remoteAddress
                },
                httpVersion: req.httpVersion,
                httpVersionMajor: req.httpVersionMajor,
                httpVersionMinor: req.httpVersionMinor,
                _startAt : _startAt
            });
            setTimeout(()=> {
                const fakeRes = Object.assign({ }, {
                    statusCode: 200
                });
                writeAccessLog(fakeReq, fakeRes, _startAt, process.hrtime());
                res.status(200).send();
            }, 500);
        });
        passport.use(STUDENTS_BEARER);
        // noinspection JSCheckFunctionSignatures
        let response = await request(app)
            .get('/test/request')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.equal(response.status, 200);
    });

});
