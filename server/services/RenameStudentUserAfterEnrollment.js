import { DataError, DataNotFoundError, TraceUtils } from '@themost/common';
import { DataObjectState } from '@themost/data';
import { ApplicationServiceAsListener } from './application-service-as-listener';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			TraceUtils.error(err);
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	// operate only on insert
	if (event.state !== DataObjectState.Insert) {
		return;
	}
	const context = event.model.context;
	// get student
	const student = await context
		.model('Student')
		.where('id')
		.equal(event.target.id)
		.select(
			'id',
			'user',
			'user/name as username',
			'studentIdentifier',
			'inscriptionYear',
			'department',
			'person'
		)
		.expand(
			{
				name: 'person',
				options: {
					$expand: 'locales',
				},
			},
			'department'
		)
		.getTypedItem();
	if (student == null) {
		throw new DataNotFoundError(
			'The specified student cannot be found or is innaccessible.'
		);
	}
	// operate only if student is linked with a user on insert
	const studentUser = student.user || event.target.user;
	if (!studentUser) {
		// if not, exit
		return;
	}
	// get institute configuration
	const instituteConfiguration = await context
		.model('InstituteConfiguration')
		.where('institute')
		.equal(student.department && student.department.organization)
		.select(
			'id',
			'usernameFormat',
			'usernameFormatSource',
			'disableExternalUserAfterRename'
		)
		.expand('usernameFormatSource')
		.silent()
		.getItem();
	if (
		!(instituteConfiguration && instituteConfiguration.usernameFormatSource)
	) {
		return;
	}
	// get department configuration
	const departmentConfiguration = await context
		.model('DepartmentConfiguration')
		.where('department')
		.equal(student.department && student.department.id)
		.select('id', 'studentUsernameFormat', 'studentUsernameIndex')
		.silent()
		.getItem();
	let username,
		studentUsernameFormat,
		studentUsernameIndex,
		updateUsernameIndex;
	switch (instituteConfiguration.usernameFormatSource.alternateName) {
		case 'none':
			TraceUtils.info(
				`[RenameStudentUserAfterEnrollment] - Student's ${student.id} user will not be renamed, as it is declared by the institute configuration.`
			);
			break;
		case 'department':
			TraceUtils.info(
				`[RenameStudentUserAfterEnrollment] - Renaming student's ${student.id} user based on their department username format.`
			);
			try {
				studentUsernameFormat = departmentConfiguration?.studentUsernameFormat;
				studentUsernameIndex = departmentConfiguration?.studentUsernameIndex;
				// generate username based on the department's username format
				username = student.generateUsername(
					studentUsernameFormat,
					studentUsernameIndex,
					student
				);
				updateUsernameIndex = true;
			} catch (err) {
				throw new DataError('E_GENERATE_USERNAME', err.message);
			}
			break;
		case 'custom':
			TraceUtils.info(
				`[RenameStudentUserAfterEnrollment] - Renaming student's ${student.id} user based on institute configuration's username format.`
			);
			try {
				studentUsernameFormat = instituteConfiguration.usernameFormat;
				// generate username based on the institute configuration's username format
				username = student.generateUsername(
					studentUsernameFormat,
					null,
					student
				);
			} catch (err) {
				throw new DataError('E_GENERATE_USERNAME', err.message);
			}
			break;
		default:
			throw new DataError('Unsupported/Invalid username format source.');
	}
	if (!(username && username.length)) {
		// if username is empty, exit
		return;
	}
	// check if a user with that username already exists
	let exists = await context
		.model('User')
		.where('name')
		.equal(username)
		.select('id')
		.silent()
		.count();
	if (exists) {
		const dynamicElements = ['U'];
		const containsDynamicElements = studentUsernameFormat
			.split(';')
			.find((formatElement) => dynamicElements.includes(formatElement));
		if (!containsDynamicElements) {
			let index = 0;
			do {
				index++;
				exists = await context
					.model('User')
					.where('name')
					.equal(username + index.toString())
					.select('id')
					.silent()
					.count();
			} while (exists);
			// append the final index
			username += index.toString();
		} else {
			// format contains at least one dynamic element, so recreate a username
			do {
				username = student.generateUsername(
					studentUsernameFormat,
					studentUsernameIndex,
					student
				);
				exists = await context
					.model('User')
					.where('name')
					.equal(username)
					.select('id')
					.silent()
					.count();
			} while (exists);
		}
	}
	// and finally update the student's user
	const updateStudentUser = {
		id: studentUser.id || studentUser,
		name: username,
		$state: 2,
	};
	// note: Use the UserReference model which is declared as a server model
	// User model with UserExternalBase replacement can also be used
	await context.model('UserReference').silent().save(updateStudentUser);

	if (updateUsernameIndex) {
		// update department configuration index, if it has been used
		departmentConfiguration.studentUsernameIndex += 1;
		await context
			.model('DepartmentConfiguration')
			.save(departmentConfiguration);
	}

	// after all in-transaction operations have been completed
	// check if disable external user option is on
	const disableExternalUser =
		instituteConfiguration.disableExternalUserAfterRename;
	if (disableExternalUser) {
		// check if student's user came from a CreateCandidateUserAction (e.g. temporary oauth2 user)
		const createCandidateUserAction = await context
			.model('CreateCandidateUserAction')
			.where('object')
			// user has been renamed, search object by id
			.equal(updateStudentUser.id)
			.and('actionStatus/alternateName')
			.equal('CompletedActionStatus')
			.select('id')
			.silent()
			.count();
		if (!createCandidateUserAction) {
			// if not, exit
			return;
		}
		// get oauth2 client service
		const service = context
			.getApplication()
			.getService(function OAuth2ClientService() {});
		// validate getUser and updateUser functions
		if (
			!(
				typeof service.getUser === 'function' &&
				typeof service.updateUser === 'function'
			)
		) {
			throw new DataError(
				'E_SERVICE',
				'Cannot disable OAuth user. The operation is unsupported by the oauth2 client service.'
			);
		}

		// get access token (for admin account)
		const adminAccount = service.settings.adminAccount;
		if (adminAccount == null) {
			throw new DataError(
				'E_MISSING_CONFIGURATION',
				'Cannot disable OAuth user. The operation cannot be completed due to missing configuration.'
			);
		}
		// authorize user and get token
		const authorizeUser = await service.authorize(adminAccount);
		// get user from oauth2 server
		let OAuthUser = await service.getUser(student.username, authorizeUser);
		if (OAuthUser == null) {
			TraceUtils.log(
				`[RenameStudentUserAfterEnrollment] - OAuth user ${student.username} (local id ${student.user}) of student with id ${student.id} has not been found.`
			);
			return;
		}
		if (OAuthUser.enabled) {
			// disable user
			OAuthUser.enabled = false;
			// and update
			await service.updateUser(OAuthUser, authorizeUser);
		}
	}
}

export class RenameStudentUserAfterEnrollment extends ApplicationServiceAsListener {
	constructor(app) {
		super(app);
		// install this listener as member of event listeners of Student model
		this.install('Student', __filename);
	}
}
