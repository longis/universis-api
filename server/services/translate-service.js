import {ApplicationService, Args} from "@themost/common";
import i18n from 'i18n';

export class TranslateService extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        this.defaultLocale = this.getApplication().getConfiguration().getSourceAt('settings/i18n/defaultLocale') || 'en';
    }

    /**
     * Translates the given phrase by getting the default application locale
     * @param {string} phrase
     * @param {*=} params
     */
    translate(phrase, params) {
        return i18n.__({
            locale: this.defaultLocale,
            phrase: phrase
        }, params);
    }

    /**
     * Translates the given phrase by getting the default application locale
     * Alias of TranslateService.translate(string, any) method
     * @param {string} phrase
     * @param {*=} params
     */
    get(phrase, params) {
        return i18n.__({
            locale: this.defaultLocale,
            phrase: phrase
        }, params);
    }

    /**
     * Sets a translation key for given locale (or the default locale)
     * @param {string} phrase
     * @param {string} translation
     * @param {string=} locale
     */
    set(phrase, translation, locale) {
        Args.notString(phrase, 'Phrase');
        Args.notString(translation, 'Translation');
        const catalog = i18n.getCatalog(locale || this.defaultLocale);
        Object.defineProperty(catalog, phrase, {
            value: translation,
            enumerable: true,
            configurable: true
        });
    }

    /**
     *
     * @param {string} locale
     * @param {*} translations
     */
    setTranslation(locale, translations) {
        const catalog = i18n.getCatalog(locale);
        if (translations != null) {
            Object.assign(catalog, translations);
        }
    }

}
