/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return DepartmentSnapshotListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

class DepartmentSnapshotListener {
    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        if (event.state === 1) {
            // check if department has reportVariables and save all
            if (target.reportVariables && target.reportVariables.length) {
                target.reportVariables = target.reportVariables.map(x => {
                    x.department = target.id;
                    delete x.id;
                    return x;
                });
                await context.model('DepartmentReportVariableSnapshot').save(target.reportVariables);
            }
            // save locales
            if (target.locales && target.locales.length) {
                target.locales = target.locales.map(x => {
                    x.object = target.id;
                    delete x.id;
                    return x;
                });
              //  await context.model('DepartmentSnapshotLocale').save(target.locales);
            }

        }
    }
}
