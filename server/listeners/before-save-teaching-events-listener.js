/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state === 1 && event.target.hasOwnProperty('eventHoursSpecification') && typeof event.target.eventHoursSpecification === "object") {
        try {
            event.target.eventHoursSpecification = (await event.model.context.model('EventHoursSpecification').save(event.target.eventHoursSpecification)).id;
        } catch (e) {
            return new Error('Could not save event hours specification ')
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
