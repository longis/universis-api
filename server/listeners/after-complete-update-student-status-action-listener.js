import ActionStatusType from '../models/action-status-type-model';
import { DataObjectState } from '@themost/data';
import { DataError, DataNotFoundError } from '@themost/common';
import _ from 'lodash';

/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
	// operate only on insert
	if (event.state !== DataObjectState.Insert) {
		return;
	}
	const context = event.model.context;
	const target = event.model.convert(event.target);
	const studentId =
		typeof target.object === 'object' ? target.object.id : target.object;
	// get student
	const student = await context
		.model('Student')
		.where('id')
		.equal(studentId)
		.select(
			'id',
			'studentStatus/alternateName as studentStatus',
			'studentIdentifier',
			'person/familyName as familyName',
			'person/givenName as givenName'
		)
		.getItem();

	if (student == null) {
		throw new DataNotFoundError(
			'The specified student cannot be found or is inaccessible',
			null,
			'Student'
		);
	}
	// validate current student status
	const allowedStudentStatuses = [
		'erased',
		'declared',
		'graduated',
		'suspended',
	];
	if (!allowedStudentStatuses.includes(student.studentStatus)) {
		throw new DataError(
			'E_STATUS',
			`The update student status action can only be performed upon erased, suspended, declared or graduated students. Target student ${
				student.id
			} is ${student.studentStatus || 'empty status'}.`,
			null,
			'UpdateStudentStatusAction',
			'studentStatus'
		);
	}
	// get new student status
	if (target.studentStatus == null) {
		throw new DataError(
			'E_TARGET_STATUS',
			'The target student status cannot be empty at this context',
			null,
			'UpdateStudentStatusAction',
			'studentStatus'
		);
	}
	// try alternateName first which is most likely to be passed, then id
	const newStudentStatus =
		(await context
			.model('StudentStatus')
			.where('alternateName')
			.equal(target.studentStatus.alternateName)
			.getItem()) ||
		(await context
			.model('StudentStatus')
			.where('id')
			.equal(target.studentStatus.id || target.studentStatus)
			.getItem());
	const allowedTargetStatuses = ['active', 'declared', 'graduated'];
	// and validate it
	if (
		!allowedTargetStatuses.includes(
			newStudentStatus && newStudentStatus.alternateName
		)
	) {
		throw new DataError(
			'E_TARGET_STATUS',
			`The update student status action can only be performed for active, declared or graduated target student status. Current target status is ${
				newStudentStatus && newStudentStatus.alternateName
			}`,
			null,
			'UpdateStudentStatusAction',
			'studentStatus'
		);
	}
	// perform some minor validations for graduated target status
	if (newStudentStatus.alternateName === 'graduated') {
		// validate that current student status is declared
		if (student.studentStatus !== 'declared') {
			throw new DataError(
				'E_TARGET_STATUS',
				`The update student status action to graduated target status can only be performed for declared students. Current student is ${
					student.studentStatus || 'empty status'
				}.`,
				null,
				'UpdateStudentStatusAction',
				'studentStatus'
			);
		}
	}
	// perform some minor validations for declared target status
	if (newStudentStatus.alternateName === 'declared') {
		// validate that current student status is graduated
		if (student.studentStatus !== 'graduated') {
			throw new DataError(
				'E_TARGET_STATUS',
				`The update student status action to declared target status can only be performed for graduated students. Current student is ${
					student.studentStatus || 'empty status'
				}.`,
				null,
				'UpdateStudentStatusAction',
				'studentStatus'
			);
		}
	}
	// set action description
	event.target.description = `${context.__('Change student status')}: [${
		student.studentIdentifier
	}] ${student.familyName} ${student.givenName} (${
		student.studentStatus
	}) ->  (${newStudentStatus.alternateName})`;
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	// operate only on update
	if (event.state !== DataObjectState.Update) {
		return;
	}
	const context = event.model.context;
	const target = event.model.convert(event.target);
	const previous = event.previous;
	// get action status
	const actionStatus = await target.property('actionStatus').getItem();
	// if action status is other than completed
	if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
		// exit
		return;
	}
	// get previous action status
	const previousActionStatus = previous && previous.actionStatus;
	if (previousActionStatus == null) {
		throw new DataError(
			'E_PREVIOUS_STATUS',
			'The previous action status cannot be empty at this context.',
			null,
			'UpdateStudentStatusAction'
		);
	}
	// validate previous status
	if (
		previousActionStatus.alternateName !== ActionStatusType.ActiveActionStatus
	) {
		// throw error for invalid previous action status
		throw new DataError(
			'E_STATUS_STATE',
			'Invalid action state. The action cannot be completed due to its previous state.',
			null,
			'UpdateStudentStatusAction'
		);
	}
	let updateStudent = true;
	// get student (will always exist due to previous validation on before save listener)
	const student = await target.property('object').getItem();
	// cover status cases to update student
	const Student = context.model('Student');
	// get target status (has been validated before save)
	const targetStatus = await target.property('studentStatus').getItem();
	// if it is active
	if (targetStatus && targetStatus.alternateName === 'active') {
		// switch on student status
		if (student.studentStatus.alternateName === 'erased') {
			// get removal attributes that are transfered from remove action
			const StudentRemoveAction = context.model('StudentRemoveAction');
			const removalAttributes = StudentRemoveAction.fields
				.filter((removalAttribute) => {
					return (
						!removalAttribute.primary &&
						removalAttribute.model === 'StudentRemoveAction' &&
						Student.fields.find((field) => field.name === removalAttribute.name)
					);
				})
				.map((removalAttribute) => removalAttribute.name);
			// clear all removal attributes of student
			removalAttributes.forEach((removalAttribute) => {
				student[removalAttribute] = null;
			});
		} else if (student.studentStatus.alternateName === 'declared') {
			// get declaration attributes that are transfered from the declare action
			const StudentDeclareAction = context.model('StudentDeclareAction');
			const declarationAttributes = StudentDeclareAction.fields
				.filter((declarationAttribute) => {
					return (
						!declarationAttribute.primary &&
						declarationAttribute.model === 'StudentDeclareAction' &&
						Student.fields.find(
							(field) => field.name === declarationAttribute.name
						)
					);
				})
				.map((declarationAttribute) => declarationAttribute.name);
			// but apply some extra (later calculated) attributes
			Array.prototype.push.apply(declarationAttributes, [
				'graduationRankByInscriptionYear',
				'graduationRankByGraduationYear',
				'graduationEvent',
			]);
			// clear all declaration attributes of student
			declarationAttributes.forEach((declarationAttribute) => {
				student[declarationAttribute] = null;
			});
			// try to find and cancel the latest student declare action
			const studentDeclareAction = await context
				.model('StudentDeclareAction')
				.where('object')
				.equal(student.id)
				.and('actionStatus/alternateName')
				.notEqual(ActionStatusType.CancelledActionStatus)
				.orderByDescending('id')
				.select('id')
				.silent()
				.getItem();
			if (studentDeclareAction) {
				// assign cancelled status
				Object.assign(studentDeclareAction, {
					actionStatus: {
						alternateName: ActionStatusType.CancelledActionStatus,
					},
					$state: 2,
				});
				// and update
				await context.model('StudentDeclareAction').save(studentDeclareAction);
			} else {
				// find and remove the declaration for this student
				const studentDeclaration = await context
					.model('StudentDeclaration')
					.where('student')
					.equal(student.id)
					.select('id')
					.silent()
					.getItem();
				if (studentDeclaration) {
					await context.model('StudentDeclaration').remove(studentDeclaration);
				}
			}
		} else if (student.studentStatus.alternateName === 'graduated') {
			// get graduation attributes that are transfered from the graduate action
			const StudentGraduateAction = context.model('StudentGraduateAction');
			const graduationAttributes = StudentGraduateAction.fields
				.filter((graduationAttribute) => {
					return (
						!graduationAttribute.primary &&
						graduationAttribute.model === 'StudentGraduateAction' &&
						Student.fields.find(
							(field) => field.name === graduationAttribute.name
						)
					);
				})
				.map((graduationAttribute) => graduationAttribute.name);
			// but apply extra (later calculated) attributes
			Array.prototype.push.apply(graduationAttributes, [
				'graduationRankByInscriptionYear',
				'graduationRankByGraduationYear',
				'graduationEvent',
			]);
			// clear all graduation attributes of student
			graduationAttributes.forEach((graduationAttribute) => {
				student[graduationAttribute] = null;
			});
		} else if (student.studentStatus.alternateName === 'suspended') {
			// do nothing
			updateStudent = false;
		} else {
			throw new DataError(
				'E_STATUS',
				`The update student status action can only be performed upon erased, suspended, declared or graduated students. Target student ${student.id} is ${student.studentStatus.alternateName}.`,
				null,
				'UpdateStudentStatusAction',
				'studentStatus'
			);
		}
	} else if (targetStatus && targetStatus.alternateName === 'graduated') {
		// first of all, try to follow the standard procedure
		// try to find the last-non cancelled-student declare action
		const studentDeclareAction = await context
			.model('StudentDeclareAction')
			.where('object')
			.equal(student.id)
			.and('actionStatus/alternateName')
			.notEqual(ActionStatusType.CancelledActionStatus)
			.orderByDescending('id')
			.select('id', 'actionStatus/alternateName as status', 'initiator')
			.silent()
			.getItem();
		// if that action exists
		if (studentDeclareAction) {
			// and it is not completed
			if (
				studentDeclareAction.status !== ActionStatusType.CompletedActionStatus
			) {
				// trigger the listeners by completing it
				studentDeclareAction.actionStatus = {
					alternateName: ActionStatusType.CompletedActionStatus,
				};
				// clear aliased field
				delete studentDeclareAction.status;
				// and update
				await context.model('StudentDeclareAction').save(studentDeclareAction);
			}
			// at this moment, a StudentGraduateAction has been created
			// and it needs to be completed, so try to find it
			// note: use initiator
			const studentGraduateAction = await context
				.model('StudentGraduateAction')
				.where('actionStatus/alternateName')
				.equal(ActionStatusType.ActiveActionStatus)
				.or('actionStatus/alternateName')
				.equal(ActionStatusType.CancelledActionStatus)
				.and('initiator')
				.equal(studentDeclareAction.initiator)
				.silent()
				.getItem();
			// if this action does not exist, stop the transaction and roll back
			if (studentGraduateAction == null) {
				throw new DataError(
					'E_GRADUATE_ACTION',
					'The process cannot be completed because the student graduate action did not get properly created or it is inaccessible.'
				);
			}
			// complete the action
			studentGraduateAction.actionStatus = {
				alternateName: ActionStatusType.CompletedActionStatus,
			};
			// and update
			await context.model('StudentGraduateAction').save(studentGraduateAction);
			// at this moment there is not need for this action to update the student
			// because the listeners already have, so set the flag
			updateStudent = false;
		} else {
			// try to find student declaration
			const studentDeclaration = await context
				.model('StudentDeclaration')
				.where('student')
				.equal(student.id)
				.silent()
				.getItem();
			// if it exists
			if (studentDeclaration) {
				// transfer calculated attributes
				student.declaredYear = studentDeclaration.declaredYear;
				student.declaredDate = studentDeclaration.declaredDate;
				student.declaredPeriod = studentDeclaration.declaredPeriod;
				student.graduationDate = studentDeclaration.graduationDate;
				student.graduationPeriod = studentDeclaration.declaredPeriod;
				student.graduationYear = studentDeclaration.declaredYear;
				student.graduationGrade = studentDeclaration.graduationGrade;
				student.graduationGradeAdjusted =
					studentDeclaration.graduationGradeAdjusted;
				student.graduationGradeScale = studentDeclaration.graduationGradeScale;
				student.graduationGradeWrittenInWords =
					studentDeclaration.graduationGradeWrittenInWords;
				// and remove student declaration
				await context.model('StudentDeclaration').remove(studentDeclaration);
				// only if graduation number has not been already calculated
				if (
					student.hasOwnProperty('graduationNumber') &&
					student.graduationNumber == null
				) {
					// generate next graduation number and assign it to student
					if (student.department == null) {
						throw new DataError(
							'E_EMPTY_DEPARTMENT',
							"The student's department cannot be empty."
						);
					}
					// get graduationNumberIndex from LocalDepartment
					const graduationNumberIndex = await context
						.model('LocalDepartment')
						.where('id')
						.equal(student.department)
						.select('graduationNumberIndex')
						.silent()
						.value();
					// only if index is not null (note: allow the value 0)
					if (graduationNumberIndex != null) {
						// assign the next graduation number to target
						student.graduationNumber = graduationNumberIndex + 1;
						// and update the local department index
						const updateGraduationNumberIndex = {
							id: student.department,
							graduationNumberIndex: graduationNumberIndex + 1,
						};
						await context
							.model('LocalDepartment')
							.silent()
							.save(updateGraduationNumberIndex);
					}
				}
			} else {
				// throw error
				throw new DataError(
					'INV_DATA',
					'The declaration cannot be completed because a StudentDeclaration is missing.'
				);
			}
		}
	} else if (targetStatus && targetStatus.alternateName === 'declared') {
		// first of all try to follow the standard procedure
		// try to find the last completed student graduate action
		const studentGraduateAction = await context
			.model('StudentGraduateAction')
			.where('object')
			.equal(student.id)
			.and('actionStatus/alternateName')
			.equal(ActionStatusType.CompletedActionStatus)
			.orderByDescending('id')
			.silent()
			.getItem();
		// if this action exists
		if (studentGraduateAction) {
			// cancel it
			studentGraduateAction.actionStatus = {
				alternateName: ActionStatusType.CancelledActionStatus,
			};
			// and update
			await context.model('StudentGraduateAction').save(studentGraduateAction);
			// listeners triggered by the above update will handle the status change to declared
			// so no need to update the student here, set the flag
			updateStudent = false;
		} else {
			// if there is no graduate action
			// it is safe to assume that there is no declare action also
			// so, create a student declaration from data fetched from the student
			const StudentDeclaration = context.model('StudentDeclaration');
			const declarationAttributes = StudentDeclaration.fields
				.filter((declarationAttribute) => {
					return (
						!declarationAttribute.primary &&
						declarationAttribute.model === 'StudentDeclaration' &&
						Student.fields.find(
							(field) => field.name === declarationAttribute.name
						)
					);
				})
				.map((declarationAttribute) => declarationAttribute.name);
			// pick the declaration attributes
			const studentDeclaration = _.pick(student, declarationAttributes);
			// assign some extra attributes
			studentDeclaration.student = student.id;
			studentDeclaration.declaredYear = student.graduationYear;
			studentDeclaration.declaredPeriod = student.graduationPeriod;
			// and save
			await context
				.model('StudentDeclaration')
				.silent()
				.save(studentDeclaration);
		}
	} else {
		throw new DataError(
			'E_TARGET_STATUS',
			`The update student status action can only be performed for active or graduated target student status. Current target status is ${
				targetStatus && targetStatus.alternateName
			}`,
			null,
			'UpdateStudentStatusAction',
			'studentStatus'
		);
	}
	if (updateStudent) {
		// assign target status to student
		Object.assign(student, {
			studentStatus: targetStatus,
			$state: 2,
		});
		// and finally update student
		await context.model('Student').save(student);
	}
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	// execute async method
	return afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
	// execute async method
	return beforeSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
