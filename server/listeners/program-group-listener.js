/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */

import {LangUtils} from "@themost/common/utils";
import {DataConflictError} from "../errors";
import {DataModel} from "@themost/data";

export function beforeSave(event, callback) {
    return ProgramGroupEventListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

export function afterExecute(event, callback) {
    return ProgramGroupEventListener.afterExecuteAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return ProgramGroupEventListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return ProgramGroupEventListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

class ProgramGroupEventListener {

    static async beforeSaveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.target.context;
        // add study program gradeScale
        if (event.state ===1) {
            event.target.gradeScale = event.target.gradeScale || event.target.program.gradeScale;
            if (!event.target.gradeScale) {
                //load from program
                event.target.gradeScale = await context.model('StudyProgram').where('id').equal(event.target.program).select('gradeScale').value();
            }
        }
        // check availableInExamsExpanded is supplied and convert to powers of two
        const availableInExams = event.target.availableInExamsExpanded;
        event.target.availableInExams = 0;
        if (availableInExams && availableInExams.length > 0) {
            for (let i = 0; i < availableInExams.length; i++) {
                const semester = availableInExams[i];
                if (semester.id === -1) {
                    event.target.availableInExams = 0;
                    break;
                }
                event.target.availableInExams += Math.pow(2, LangUtils.parseFloat(semester.id - 1));
            }
        }

    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        // check previous and current parent group
        const context = event.model.context;
        if (event.state === 2) {
            // cannot change parent group
            const previousParentGroup = event.previous && event.previous.parentGroup;
            let parentGroup = event.target.parentGroup;
            if (parentGroup!=null) {
                parentGroup = context.model('ProgramGroup').convert(event.target.parentGroup).getId();
            }
            // validate previous status
            if (previousParentGroup != null && parentGroup !== previousParentGroup) {
                // check student groups
                const exists = await context.model('StudentProgramGroup').where('programGroup').equal(event.target.id).silent().count();
                if (exists) {
                    throw new DataConflictError(context.__('Cannot change parent group because it has been registered by students'));
                }
            }
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterExecuteAsync(event) {

        // add available attachment types related with graduationEvent (only if one record is returned)
        let data = event['result'];
        // validate data
        if (data == null) {
            return;
        }
        // validate query and exit
        if (event.query.$group) {
            return;
        }
        if (!(Array.isArray(data) && data.length)) {
            return;
        }
        const context = event.model.context;
        if (data.length === 1) {
            if (data[0].availableInExams) {
                // get powers of two
                const semesters = this.getPowersOfTwo(data[0].availableInExams).map(semester => {
                    return semester += 1;
                });
                data[0].availableInExamsExpanded = await context.model('Semesters').where('id').in(semesters).getItems();
            }
        }
        return data;
    }

    static getPowersOfTwo(value) {
        let b = 1;
        let res = [];
        while (b <= value) {
            if (b & value) {
                res.push(this.getPower(2, b));
            }
            b <<= 1;
        }
        return res;
    }

    static getPower(x, y) {
        // Repeatedly compute power of x
        let pow = 1;
        let power = 0;
        while (pow < y) {
            pow = pow * x;
            power += 1;
        }
        // Check if power of x becomes y
        if (pow === y)
            return power;
    }

    /**
     * @param {DataEventArgs} event
     */
    static async beforeRemoveAsync(event) {
        const model = event.model;
        const getReferenceMappings = DataModel.prototype.getReferenceMappings;
        model.getReferenceMappings = async function () {
            const res = await getReferenceMappings.bind(this)();
            // remove readonly model StudentAvailableClasses from mapping before delete
            const mappings = ['ProgramGroupRuleExCheckValues','StudentAvailableClass'];
            return res.filter((mapping) => {
                return mappings.indexOf(mapping.childModel) < 0;
            });
        };
        // update program courses, set programGroup to null
        const programCourseModel = await event.model.context.model('StudyProgramCourse');
        let programCourses = await programCourseModel.where('programGroup').equal(event.target.id).getItems();
        if (programCourses && programCourses.length>0)
        {
            // set program group to null and save
            programCourses = programCourses.map(x=>{
                x.programGroup=null;
                return x;
            });
            await programCourseModel.save(programCourses);

        }
    }

}
