import {GenericDataConflictError, ValidationResult} from "../errors";
import {Args, DataNotFoundError, TraceUtils} from "@themost/common";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return ValidateStudentRequestTypeListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return callback();
}

class ValidateStudentRequestTypeListener {

    static async beforeSaveAsync(event) {
        // get context
        const context = event.model.context;

        if (event.state != 1) {
            return;
        }
        const student = context.model('Student').convert(event.target.student);
        const language = event.target.inLanguage || context.locale;
        Args.notNull(student, 'Student');
        // get request type configuration
        const config = await context.model('StudentRequestConfiguration')
            .where('additionalType').equal(event.model.name)
            .and('inLanguage').equal(language)
            .silent()
            .getItem();

        if (!config) {
            // rules have not been set for this request type
            return;
        }
        if (config.validationResult && config.validationResult.success===false) {
            throw new GenericDataConflictError('RULE', config.validationResult.message, null,
                'StudentRequestAction');
        }
        // check maximum active requests for this student

        if (config.maximumActive > 0) {
            const active = await context.model(event.model.name)
                .where('student').equal(student.id)
                .and('actionStatus/alternateName').equal('ActiveActionStatus')
                .silent().count();
            if (active >= config.maximumActive) {
                throw new GenericDataConflictError('ERR_ACTIVE_ACTION_MAX_REACHED', context.__('Number of maximum allowed active requests has been reached for this request type'), null,
                    'StudentRequestAction');
            }
        }
        // check maximum completed requests

        if (config.maximumAllowed > 0) {
            const completed = await context.model(event.model.name)
                .where('student').equal(student.id)
                .and('actionStatus/alternateName').equal('CompletedActionStatus')
                .silent().count();

            if (completed >= config.maximumAllowed) {
                throw new GenericDataConflictError('ERR_COMPLETED_ACTION_MAX_REACHED', context.__('Number of maximum allowed completed requests has been reached for this request type'), null,
                    'StudentRequestAction');
            }
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        return;
    }
}
