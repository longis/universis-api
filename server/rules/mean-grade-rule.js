import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import util from "util";
import Rule from "./../models/rule-model";
import CourseAttribute from "./../models/course-attribute-rule";
import CourseType from '../models/course-type-model';
import Semester from '../models/semester-model';
import {round} from "mathjs";


/**
 * @class
 * @augments Rule
 */
export default class MeanGradeRule extends CourseAttribute {
    constructor(obj) {
        super('Rule', obj);
    }

    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {function(Error=,*=)} done
     */
    validate(obj, done) {
        try {
            const self = this;
            const context = self.context;
            const students = context.model('Student'), student = students.convert(obj.student),
                studentCourses = context.model('StudentCourse');
            if (_.isNil(student)) {
                return done(null, self.failure('ESTUD', 'Student data is missing.'));
            }
            if (_.isNil(student.getId())) {
                return done(null, self.failure('ESTUD', 'Student data cannot be found.'));
            }
            //get queryable object completed courses
            const q = studentCourses.where('student').equal(student.getId()).and('isPassed').equal(1).and('courseStructureType').in([1, 4])
                .expand('semester', {
                    'name': 'course',
                    'options': {
                        '$expand': 'gradeScale'
                    }
                }).prepare();
            if (self.value1) {
                //TODO: add also examPeriod
                const examYears = this.value1.split(',').map(x => {
                    return x.split('-')[0];
                });
                q.where('gradeYear').in(examYears).prepare();
            }
            if (self.value2) {
                //TODO: add also registrationPeriod
                const registrationYears = this.value2.split(',').map(x => {
                    return x.split('-')[0];
                });
                q.where('lastRegistrationYear').in(registrationYears).prepare();
            }
            if (this.checkValues !== "-1") {
                //filter specific courseTypes, -1 all types
                q.where('semester').in(this.checkValues.split(',')).prepare();
            }
            // add check for calculateUnits if rule additionalType is GraduateRule
            if (this.additionalType === 'GraduateRule') {
                q.where('calculateUnits').equal(1).prepare();
            }
            if (this.additionalType === 'ScholarshipRule') {
                q.where('course/isCalculatedInScholarship').equal(1).prepare();
            }
            //exclude specific courses
            if (!_.isNil(this.value7)) {
                q.where("course").notIn(this.value7.split(',')).prepare();
            }
            //exclude specific course types
            if (!_.isNil(this.value8)) {
                q.where("courseType").notIn(this.value8.split(',')).prepare();
            }
            //check also course category
            if (!_.isNil(this.value9)) {
                q.where("course/courseCategory").in(this.value9.split(',')).prepare();
            }
            //check also course sector
            if (!_.isNil(this.value10)) {
                q.where("course/courseSector").in(this.value10.split(',')).prepare();
            }
            //check also course area
            if (!_.isNil(this.value11)) {
                q.where("course/courseArea").in(this.value11.split(',')).prepare();
            }
            q.silent().all(function (err, studentCourses) {
                if (err) {
                    TraceUtils.error(err);
                    return done(null, self.failure('EFAIL', 'An error occured while trying to validate rules.'));
                }
                let totalCourses = [];
                // get required number of courses per coursetype
                if (self.value5) {
                    // split courseTypes 1;4#2;5
                    const types = self.value5.split('#') || [];
                    if (types && types.length > 0) {
                        for (let j = 0; j < types.length; j++) {
                            const result = types[j].split(';');
                            if (result && result[0]) {
                                totalCourses.push({
                                    courseType: LangUtils.parseFloat(result[0]),
                                    courses: LangUtils.parseFloat(result[1])
                                });
                            }
                        }
                    }
                }
                // get parameters of ruleCheckType
                self.getRuleMeanGradeTypes(function (err, ruleTypes) {
                    if (err) {
                        return done(err);
                    }
                    const excludeExemption = ruleTypes.find(x => {
                        return x.alternateName === 'excludeExemption';
                    });
                    const withCoefficients = ruleTypes.find(x => {
                        return x.alternateName === 'withCoefficients';
                    });
                    const maximumMeanGrade = ruleTypes.find(x => {
                        return x.alternateName === 'maximumMeanGrade';
                    });
                    const totalUnits = ruleTypes.find(x => {
                        return x.alternateName === 'totalUnits';
                    });
                    // calculate courses that not participating in graduation degree
                    const calculateCourses = ruleTypes.find(x => {
                        return x.alternateName === 'calculateCourses';
                    });
                    // check options
                    if (excludeExemption) {
                        //exclude course exemptions
                        studentCourses = studentCourses.filter(x => {
                            return x.registrationType === 0
                        });
                    }
                    if (!calculateCourses) {
                        studentCourses = studentCourses.filter(x => {
                            return x.calculateGrade !== 0;
                        });
                    }
                    // check totals per courseType
                    for (let i = 0; i < totalCourses.length; i++) {
                        const countPerCourseType = totalCourses[i];
                        if (countPerCourseType.courseType === -1) {
                            countPerCourseType.numberOfCourses = studentCourses.length;
                            countPerCourseType.sumOfUnits = studentCourses.reduce((sumOfUnits, a) => sumOfUnits + a.units, 0);
                        } else {
                            const filtered = studentCourses.filter(x => {
                                return x.courseType.id === countPerCourseType.courseType;
                            });
                            countPerCourseType.numberOfCourses = filtered.length;
                            countPerCourseType.sumOfUnits = filtered.reduce((sumOfUnits, a) => sumOfUnits + a.units, 0);
                        }
                        // check totals
                        countPerCourseType.success = totalUnits ? countPerCourseType.sumOfUnits >= countPerCourseType.courses ? true : false : countPerCourseType.numberOfCourses >= countPerCourseType.courses ? true : false;
                    }

                    // keep result in data and pass this to validationResult
                    const data = {
                        "result": totalCourses,
                        "value1": self.value1,
                        "value2": self.value2
                    };
                    // check total courses
                    const failure = totalCourses.find(x => {
                        return x.success === false;
                    });
                    if (!failure) {
                        // calculate mean grade
                        // check if specific courseTypes are defined
                        if (self.value12) {
                            // split courseTypes 1;4#2;5
                            // clear totalCourses
                            totalCourses = [];
                            const types = self.value12.split('#') || [];
                            if (types && types.length > 0) {
                                for (let j = 0; j < types.length; j++) {
                                    const result = types[j].split(';');
                                    if (result && result[0]) {
                                        totalCourses.push({
                                            courseType: LangUtils.parseFloat(result[0]),
                                            courses: LangUtils.parseFloat(result[1])
                                        });
                                    }
                                }
                            }
                        }
                        //calculate mean grade from studentCourses
                        studentCourses = studentCourses.map(studentCourse => {
                            if (!withCoefficients) {
                                studentCourse.coefficient = 1;
                            }
                            studentCourse.coefficient = studentCourse.course.gradeScale.scaleType !== 3 ? studentCourse.coefficient : 0;
                            studentCourse.product = studentCourse.grade * studentCourse.coefficient;
                            return studentCourse;
                        });

                        if (totalCourses.length > 0) {
                            // sort by courseType
                            totalCourses = totalCourses.sort(function (a, b) {
                                return parseFloat(b.courseType) - parseFloat(a.courseType);
                            });
                            if (self.value12 || maximumMeanGrade) {
                                let calcCourses = [];
                                // filter student courses
                                // check totals per courseType, sort by product and then by coefficient and take top
                                for (let i = 0; i < totalCourses.length; i++) {
                                    const countPerCourseType = totalCourses[i];
                                    if (countPerCourseType.courseType === -1) {
                                        countPerCourseType.numberOfCourses = studentCourses.length;
                                        // sort by product descending and coeffient ascending
                                        studentCourses = studentCourses.sort(function (a, b) {
                                            return parseFloat(b.product) - parseFloat(a.product);
                                        }).sort(function (a, b) {
                                            return parseFloat(a.coefficient) - parseFloat(b.coefficient);
                                        }).splice(0, countPerCourseType.courses);
                                        countPerCourseType.numberOfCourses = studentCourses.length;
                                        countPerCourseType.sumOfUnits = studentCourses.reduce((sumOfUnits, a) => sumOfUnits + a.units, 0);

                                        break;

                                    } else {
                                        const filtered = studentCourses.filter(x => {
                                            return x.courseType.id === countPerCourseType.courseType;
                                        }).sort(function (a, b) {
                                            return parseFloat(b.product) - parseFloat(a.product);
                                        }).sort(function (a, b) {
                                            return parseFloat(a.coefficient) - parseFloat(b.coefficient);
                                        }).splice(0, countPerCourseType.courses);
                                        countPerCourseType.numberOfCourses = filtered.length;
                                        countPerCourseType.sumOfUnits = filtered.reduce((sumOfUnits, a) => sumOfUnits + a.units, 0);
                                        calcCourses.push.apply(calcCourses, filtered);
                                    }
                                }
                                if (calcCourses.length > 0) {
                                    studentCourses = calcCourses;
                                }
                            }
                        }
                        data.sumOfProduct = round(studentCourses.reduce((partial_sum, a) => partial_sum + a.product, 0), 5);
                        data.sumOfCoefficient = studentCourses.reduce((partial_sum, a) => partial_sum + a.coefficient, 0);
                        data.finalGrade = data.sumOfCoefficient > 0 ? (round(data.sumOfProduct / data.sumOfCoefficient, 3)) : 0;

                        if (data.finalGrade >= self.value3) {
                            return done(null, self.success('SUCC', "", null, data));
                        } else {
                            return done(null, self.failure('FAIL', "", null, data));
                        }
                    } else {
                        return done(null, self.failure('FAIL', "", null, data));
                    }
                });
            });
        } catch (e) {
            done(e);
        }
    }

    /**
     * Returns the comparison operator associated with this rule
     * @returns {string}
     */
    getRuleMeanGradeTypes(callback) {
        try {
            const powers = this.getPowersOfTwo(this.value4);
            const ruleCheckTypes = [];
            for (let j = 0; j < powers.length; j++) {
                ruleCheckTypes.push(Math.pow(2, powers[j]));
            }
            if (ruleCheckTypes.length === 0)
                callback(null, []);
            else {
                this.context.model('RuleMeanGradeType').where('identifier').in(ruleCheckTypes).silent().all(function (err, result) {
                    if (err) {
                        return callback(err);
                    }
                    callback(null, result);
                });
            }
        } catch (e) {
            callback(e);
        }
    }

    /**
     *
     * @param {string} targetModel
     */
    async expand(targetModel) {
        let result = {};
        const model = this.context.model(targetModel);
        // load courseTypes
        const courseTypes = await CourseType.getAllCourseTypes(this.context);
        // load semesters
        const semesters = await Semester.getAllSemesters(this.context);

        const attributes = model.attributes;
        for (let i = 0; i < attributes.length; i++) {
            const attribute = attributes[i];
            const mapping = model.inferMapping(attribute.name);
            let value;
            // get target model property name
            let name = attribute.property || attribute.name;
            if (mapping == null) {
                if (this[attribute.name] != null) {
                    if (name === 'totalCourses' || name === 'totalCoursesPerType') {
                        const totalCourses = [];
                        // split courseTypes 1;4#2;5
                        const types = this[attribute.name].split('#') || [];
                        if (types && types.length > 0) {
                            for (let j = 0; j < types.length; j++) {
                                const result = types[j].split(';');
                                if (result && result[0]) {
                                    const courseType = courseTypes.find(x => {
                                        return x.id === LangUtils.parseFloat(result[0]);
                                    });
                                    totalCourses.push({
                                        'courseType': {
                                            id: LangUtils.parseFloat(result[0]),
                                            name: courseType ? courseType.name : 'All',
                                        }, courses: LangUtils.parseFloat(result[1])
                                    });
                                }
                            }
                        }
                        this[attribute.name] = totalCourses;
                    }
                    if (name === 'semesters') {
                        const findSemesters = [];
                        // split semesters 1,2createdBy = 0
                        const result = this[attribute.name].split(',');
                        for (let j = 0; j < result.length; j++) {
                            const semester = semesters.find(x => {
                                return x.id === LangUtils.parseFloat(result[j]);
                            });
                            findSemesters.push(semester);
                        }
                        this[attribute.name] = findSemesters;
                    }
                    if (name === 'examYears' || name === 'registrationYears') {
                        // split years 2019,2020
                        const result = this[attribute.name].split(',');
                        for (let i = 0; i < result.length; i++) {
                            const examYear = result[i];
                            result[i] = `${examYear.split('-')[0]}`;
                        }
                        const years = await this.context.model('AcademicYear').where('id').in(result).silent().getItems();
                        this[attribute.name] = years;
                    }
                    if (name === 'ruleCheckType') {
                        let powers = this.getPowersOfTwo(this[attribute.name]);
                        let ruleCheckTypes = [];
                        const ruleMeanGradeTypes = await this.context.model('RuleMeanGradeType').getItems();
                        for (let j = 0; j < powers.length; j++) {
                            const result = powers[j];
                            const meanGradeType = ruleMeanGradeTypes.find(x => {
                                return x.id === LangUtils.parseFloat(result) + 1;
                            });
                            if (meanGradeType) {
                                ruleCheckTypes.push(meanGradeType);
                            }
                        }
                        this[attribute.name] = ruleCheckTypes;
                    }
                    Object.defineProperty(result, name, {
                        configurable: true, enumerable: true, writable: true, value: this[attribute.name]
                    });
                }

            } else {
                value = mapping.associationType === 'junction' ? [] : null;
                if (this[attribute.name] != null) {
                    if (mapping.associationType === 'junction') {
                        value = [];
                        if (mapping.parentModel === model.name) {
                            if (this[attribute.name] === "-1") {
                                value.push({
                                    "id": -1
                                });
                            } else {
                                const values = this[attribute.name].split(',');
                                value = await this.context.model(mapping.childModel).where(mapping.childField).in(values).getAllItems();
                            }
                        }
                    }
                    if (mapping.associationType === 'association') {
                        value = await this.context.model(mapping.parentModel).where(mapping.parentField).equal(this[attribute.name]).getItem();
                    }
                    Object.defineProperty(result, name, {
                        configurable: true, enumerable: true, writable: true, value: value
                    });
                }
            }
        }
        return result;
    }

    getPowersOfTwo(value) {
        let b = 1;
        let res = [];
        while (b <= value) {
            if (b & value) {
                res.push(this.getPower(2, b));
            }
            b <<= 1;
        }
        return res;
    }

    getPower(x, y) {
        // Repeatedly compute power of x
        let pow = 1;
        let power = 0;
        while (pow < y) {
            pow = pow * x;
            power += 1;
        }
        // Check if power of x becomes y
        if (pow === y)
            return power;
    }
}
