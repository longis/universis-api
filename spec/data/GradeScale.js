// eslint-disable-next-line no-unused-vars
import GradeScale from '../../server/models/grade-scale-model';

/**
 *
 * @type {Array<GradeScale>}
 */
const GRADE_SCALES = [
    {
        name: '0 to 10', // a scale 0-10
        scaleBase: .5, // 50% percent
        scaleType: 0, // numeric grade scale
        scaleFactor: .1,
        formatPrecision: 2,
        scalePrecision: 4
    }
]
export {
    GRADE_SCALES
};
