import faker from 'faker';
import {INSTITUTE} from './Institute';
const DEPARTMENT = {
    id: 20001,
    name: 'School of Law',
    abbreviation:'LAW',
    organization: INSTITUTE,
    currentYear: 2015,
    currentPeriod: 1,
    email: faker.internet.url(),
    url: faker.internet.email('Law', 'School'),
    studyLevel: 1,
    address: faker.address.streetAddress()
};

export {
    DEPARTMENT
};
