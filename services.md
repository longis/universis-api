# Services

## Available optional services
This is a list of the **optional** services available in API private.

1. `@universis/keycloak#KeycloakClientService`

    Use **keycloak for authentication**

1. `./services/content-service#PrivateContentService`

    Use **external folder** for content/private

1. `./services/shared-content-service#SharedContentService`

    Use **external folder** for content/shared

1. `universis-custom-registration-rules#CustomRulesPlugin`

    **Registration rules for Aristotle University**

    This is an external service provided by https://gitlab.com/universis/universis-custom-registration-rules

1. `./listeners/auto-reject-student-request-action-listener#AutoRejectStudentRequestAction`

    **Reject student requests** when the student has the status `graduated` or `declared`

1. `./listeners/validate-available-classes-listener#ValidateAvailableClassesAction`

    Provide a toggle button in course registration, to **only show courses that are available to the student**, eg. removes courses that cannot be registered by student due to student's semester

1. `./listeners/send-email-document-listener#EmailDocumentAction`

    **Send an email to student**, after a request submission.
    Used to complete requests such as a requests for a **Draft Transcript**

## Use

To use any of the available services, you have to add them in your `app.json` file under **services**.

Example of services configuration in app.json
```
{
  "services": [
    { "serviceType": "./services/oauth2-client-service#OAuth2ClientService",
      "strategyType": "@universis/keycloak#KeycloakClientService"
    },
    { "serviceType": "./services/swagger-service#SwaggerService" },
    { "serviceType": "./services/content-service#PrivateContentService" },
    { "serviceType": "./services/shared-content-service#SharedContentService" },
    { "serviceType": "universis-custom-registration-rules#CustomRulesPlugin" },
    { "serviceType": "./listeners/auto-reject-student-request-action-listener#AutoRejectStudentRequestAction" },
    { "serviceType": "./listeners/validate-available-classes-listener#ValidateAvailableClassesAction" },
    { "serviceType": "./listeners/send-email-document-listener#EmailDocumentAction" }
  ],
  ...
}
```
